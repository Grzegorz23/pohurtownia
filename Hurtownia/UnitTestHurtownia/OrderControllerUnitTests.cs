﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hurtownia.Controllers;
using System.Collections.Generic;
using Hurtownia.Models;

namespace UnitTestHurtownia
{
    [TestClass]
    public class OrderControllerUnitTests
    {
        OrderController kontroler = new OrderController();

        [TestMethod]
        public void SprawdzDostepnoscTowarowBrakTowaru()
        {
            List<PozycjaZamowienia> pozycje = new List<PozycjaZamowienia> {
                new PozycjaZamowienia {
                    IloscTowaru = 12,
                    Towar = new Towar { IloscNaMagazynie = 30 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 30,
                    Towar = new Towar { IloscNaMagazynie = 120 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 6,
                    Towar = new Towar { IloscNaMagazynie = 4 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 19,
                    Towar = new Towar { IloscNaMagazynie = 20 }
                }
            };
            bool wynik = kontroler.SprawdzDostepnoscTowarow(pozycje);
            Assert.IsFalse(wynik);
        }


        [TestMethod]
        public void SprawdzDostepnoscTowarowBrakWszystkichTowarow()
        {
            List<PozycjaZamowienia> pozycje = new List<PozycjaZamowienia> {
                new PozycjaZamowienia {
                    IloscTowaru = 12,
                    Towar = new Towar { IloscNaMagazynie = 2 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 30,
                    Towar = new Towar { IloscNaMagazynie = 0 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 6,
                    Towar = new Towar { IloscNaMagazynie = 4 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 19,
                    Towar = new Towar { IloscNaMagazynie = 18 }
                }
            };
            bool wynik = kontroler.SprawdzDostepnoscTowarow(pozycje);
            Assert.IsFalse(wynik);
        }


        [TestMethod]
        public void SprawdzDostepnoscTowarowRownaIloscTowarow()
        {
            List<PozycjaZamowienia> pozycje = new List<PozycjaZamowienia> {
                new PozycjaZamowienia {
                    IloscTowaru = 12,
                    Towar = new Towar { IloscNaMagazynie = 12 }
                }
            };
            bool wynik = kontroler.SprawdzDostepnoscTowarow(pozycje);
            Assert.IsTrue(wynik);
        }


        [TestMethod]
        public void SprawdzDostepnoscTowarowDostepneTowary()
        {
            List<PozycjaZamowienia> pozycje = new List<PozycjaZamowienia> {
                new PozycjaZamowienia {
                    IloscTowaru = 12,
                    Towar = new Towar { IloscNaMagazynie = 30 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 30,
                    Towar = new Towar { IloscNaMagazynie = 120 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 6,
                    Towar = new Towar { IloscNaMagazynie = 7 }
                },
                new PozycjaZamowienia {
                    IloscTowaru = 19,
                    Towar = new Towar { IloscNaMagazynie = 20 }
                }
            };
            bool wynik = kontroler.SprawdzDostepnoscTowarow(pozycje);
            Assert.IsTrue(wynik);
        }
    }
}

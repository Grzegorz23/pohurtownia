﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Hurtownia.Models;

namespace UnitTestHurtownia
{
    [TestClass]
    public class FakturaUnitTests
    {
        Faktura faktura = new Faktura();

        [TestMethod]
        public void SprawdzLiczenieNetto()
        {
            float cena = 22.13f;
            Assert.AreEqual(cena / 1.23f, faktura.ObliczCeneNetto(cena, 0.23f));
        }

        [TestMethod]
        public void SlownyZapisKwotyCyfra()
        {
            int cyfra = 4;
            Assert.AreEqual("cztery", faktura.SlownyZapisKwoty(cyfra));
        }

        [TestMethod]
        public void SlownyZapisKwotyLiczbaTrzycyfrowa()
        {
            int liczba = 549;
            Assert.AreEqual("pięćset czterdzieści dziewięć", faktura.SlownyZapisKwoty(liczba));
        }

        [TestMethod]
        public void SlownyZapisKwotyLiczbaTrzycyfrowaZZerami()
        {
            int liczba = 102;
            Assert.AreEqual("sto dwa", faktura.SlownyZapisKwoty(liczba));
        }

        [TestMethod]
        public void SlownyZapisKwotyRownaLiczbaCzterocyfrowa()
        {
            int liczba = 1000;
            Assert.AreEqual("tysiąc", faktura.SlownyZapisKwoty(liczba));
        }

        [TestMethod]
        public void SlownyZapisKwotyLiczbaCzterocyfrowa()
        {
            int liczba = 5616;
            Assert.AreEqual("pięć tysięcy sześćset szesnaście", faktura.SlownyZapisKwoty(liczba));
        }

        [TestMethod]
        public void SlownyZapisKwotyLiczbaPięciocyfrowaZZerami()
        {
            int liczba = 19001;
            Assert.AreEqual("dziewiętnaście tysięcy jeden", faktura.SlownyZapisKwoty(liczba));
        }
    }
}

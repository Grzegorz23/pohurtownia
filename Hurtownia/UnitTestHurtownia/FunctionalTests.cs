﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;

namespace UnitTestHurtownia
{
    [TestClass]
    public class FunctionalTests
    {
        private string baseURL = "http://localhost:50634";
        private RemoteWebDriver driver;

   

        [TestCleanup()]
        public void MyTestCleanup()
        {
            driver.Quit();
        }

        [TestInitialize()]
        public void MyTestInitialize()
        {
        }

        [TestMethod]
        public void TestZmienIloscTowaru()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Navigate().GoToUrl(baseURL);
            driver.FindElementById("PozycjezZamowienia_1__IloscTowaru").Clear();
            driver.FindElementById("PozycjezZamowienia_1__IloscTowaru").SendKeys("20");
            driver.FindElementById("zlozZamowienieBtn").Click();
        }

        [TestMethod]
        public void TestWpiszAdres()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Navigate().GoToUrl(baseURL+"/Order/Edit/4");
            driver.FindElementById("Adres_Ulica").Clear();
            driver.FindElementById("Adres_Ulica").SendKeys("Kamieniczkowa");
            driver.FindElementById("Adres_NumerDomu").Clear();
            driver.FindElementById("Adres_NumerDomu").SendKeys("7");
            driver.FindElementById("Adres_NrMieszkania").Clear();
            driver.FindElementById("Adres_NrMieszkania").SendKeys("17");
            driver.FindElementById("DalejBtn").Click();
        }

        [TestMethod]
        public void TestWybierzTypPlatnosci()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Navigate().GoToUrl(baseURL + "/Order/Payment/4");
            driver.FindElementById("GotowkaRadioBtn").Click();
            driver.FindElementById("ZlozZamowienieBtn").Click();
        }

        [TestMethod]
        public void TestGenerowanieFaktury()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(30);
            driver.Navigate().GoToUrl(baseURL + "/Order/OrdersList");
            driver.FindElementById("radiobtn 4").Click();
            driver.FindElementById("btnFaktura").Click();
            driver.FindElementById("btnFaktura").Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(300);
        }
    }
}

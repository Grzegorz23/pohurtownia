﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Pracownik
    {
        public int Id { get; set; }

        [ForeignKey("RolaID")]
        public virtual Rola rola { get; set; }
        public int RolaID { get; set; }

        public virtual ICollection<Zamowienie> Zamowienia { get; set; }
        public string Imie { get; set; }
        public string nazwisko { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Haslo { get; set; }
    }
}

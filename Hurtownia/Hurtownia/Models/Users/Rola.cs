﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Rola
    {
        public int ID { get; set; }
        public string NazwaRoli { get; set; }
        public int PoziomUprawnien { get; set; }
        public virtual ICollection<Pracownik> Pracownicy { get; set; }
    }
}

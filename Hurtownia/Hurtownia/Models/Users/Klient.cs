﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Klient
    {
        public int Id { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Email { get; set; }
        public string Login { get; set; }
        public string Haslo { get; set; }
        public string NIP { get; set; }
        public string NazwaFirmy { get; set; }
        [ForeignKey("AdresId")]
        public virtual Adres Adres { get; set; }
        public int AdresId { get; set; }
        public virtual ICollection<Platnosc> Platnosci { get; set; }
    }
}

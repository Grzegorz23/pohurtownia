﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Towar
    {
        public int ID { get; set; }
        public float Cena { get; set; }
        public string NazwaTowaru { get; set; }
        public int IloscNaMagazynie { get; set; }
        public virtual ICollection<PozycjaZamowienia> PozycjeZamowienia { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class EFDbContext : DbContext
    {
        //Konstruktor bazowy przyjmuje nazwę connectionStringa. Właściwość name connectionStringa musi być równa 
        //argumentowi konstruktora ("EFDbContext")
        public EFDbContext()
            : base("Hurtownia")
        {
        }

        public virtual IDbSet<Adres> Adresy { get; set; }
        public virtual IDbSet<DowodSprzedazy> DowodySprzedazy { get; set; }
        public virtual IDbSet<Faktura> Faktury { get; set; }
        public virtual IDbSet<Klient> Klienci { get; set; }
        public virtual IDbSet<Platnosc> Platnosci { get; set; }
        public virtual IDbSet<PozycjaDowoduSprzedazy> PozycjeDowoduSprzedazy { get; set; }
        public virtual IDbSet<PozycjaZamowienia> PozycjeZamowienia { get; set; }
        public virtual IDbSet<Pracownik> Pracownicy { get; set; }
        public virtual IDbSet<Rola> Role { get; set; }
        public virtual IDbSet<StatusZamowienia> StatusyZamowienia { get; set; }
        public virtual IDbSet<Towar> Towary { get; set; }
        public virtual IDbSet<TypPlatnosci> TypyPlatnosci { get; set; }
        public virtual IDbSet<Zamowienie> Zamowienia { get; set; }



        
    }

    
}

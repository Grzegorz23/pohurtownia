﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class PozycjaZamowienia
    {
        public int Id { get; set; }
        public int IloscTowaru { get; set; }

        public int TowarId { get; set; }
        [ForeignKey("TowarId")]
        public virtual Towar Towar { get; set; }
        public int ZamowienieId { get; set; }
        [ForeignKey("ZamowienieId")]
        public virtual Zamowienie Zamowienie { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class StatusZamowienia
    {
        public int ID { get; set; }
        public string Status { get; set; }
        public virtual ICollection<Zamowienie> Zamowienia { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class DowodSprzedazy
    {
        [Key,ForeignKey("Zamowienie")]
        public int ZamowienieId { get; set; }
        public DateTime DataWystawienia { get; set; }
        public virtual Zamowienie Zamowienie {get;set;}
        public virtual Faktura Faktura { get; set; }
        public virtual IList<PozycjaDowoduSprzedazy> PozycjezDowoduSprzedazy { get; set; }

    }
}

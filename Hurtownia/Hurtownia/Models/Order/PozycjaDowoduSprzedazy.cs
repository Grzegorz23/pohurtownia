﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class PozycjaDowoduSprzedazy
    {
        public int Id { get; set; }
        public string NazwaTowaru { get; set; }
        public int IloscTowaru { get; set; }
        public float Cena { get; set; }
        public float CenaNetto { get; set; }

        public int DowodSprzedazyId { get; set; }
        [ForeignKey("DowodSprzedazyId")]
        public virtual DowodSprzedazy DowodSprzedazy { get; set; }
    }
}

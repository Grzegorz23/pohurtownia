﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Zamowienie
    {
        public int ZamowienieId { get; set; }
        [ForeignKey("AdresId")]
        public virtual Adres Adres { get; set; }
        public int AdresId { get; set; }
       
        [ForeignKey("PracownikId")]
        public virtual Pracownik Pracownik {get;set;}
        public int PracownikId { get; set; }
        
        [ForeignKey("StatusZamowieniaId")]
        public virtual StatusZamowienia StatusZamowienia { get; set; }
        public int StatusZamowieniaId { get; set; }

        
        public virtual Platnosc Platnosc { get; set; } 
        public bool OdbiorOsobisty { get; set; }

        public virtual IList<PozycjaZamowienia> PozycjezZamowienia{ get; set; }
        public virtual DowodSprzedazy DowodSprzedazy { get; set; }
        public string NumerZamowienia { get; set; }
        public float WartoscZamowienia { get; set; }
        public DateTime DataZlozenia { get; set; }
        public DateTime DataRealizacji { get; set; }



         
    }
}

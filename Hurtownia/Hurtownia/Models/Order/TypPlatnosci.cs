﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class TypPlatnosci
    {
        public int ID { get; set; }
        public string Typ { get; set; }
        public virtual ICollection<Platnosc> Platnosci { get; set; }
    }
}

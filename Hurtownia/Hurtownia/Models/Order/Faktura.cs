﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    
    public class Faktura
    {

        [Key,ForeignKey("DowodSprzedazy")]
        public int ZamowienieId { get; set; }

        public string KwotaFaktury { get; set; }
        public string NIP { get; set; }
        public string NazwaFirmy { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Ulica { get; set; }
        public int NumerDomu { get; set; }
        public int NumerMieszkania { get; set; }
        public string Miasto { get; set; }
        public string KodPocztowy { get; set; }
        public virtual DowodSprzedazy DowodSprzedazy { get; set; }

        /// <summary>
        /// Metoda obliczjąca cenę netto kwoty podanej jako cena, za pomocą wartościVat.
        /// </summary>
        /// <param name="cena">Cena brutto</param>
        /// <param name="wartoscVat">podatek VAT jako liczba dziesiętna</param>
        /// <returns></returns>
        public float ObliczCeneNetto(float cena, float wartoscVat)
        {
            return cena / (1 + wartoscVat);
        }

        /// <summary>
        /// Metoda zapisująca słownie zapisaną kwotę pobraną za zamówienia do pola faktury "KwotaFaktury"
        /// </summary>
        public void ZapiszSlownaKwote()
        {
            string zapisSlowny = SlownyZapisKwoty((int)DowodSprzedazy.Zamowienie.WartoscZamowienia);
            zapisSlowny = zapisSlowny.First().ToString().ToUpper() + zapisSlowny.Substring(1);
            KwotaFaktury = zapisSlowny;
        }

        /// <summary>
        /// Metoda przekształcająca kwotę podaną jako argument na zapis słowny
        /// </summary>
        /// <param name="kwota">kwota</param>
        /// <returns>Zapis słowny</returns>
        public string SlownyZapisKwoty(int kwota)
        {
            string[] cyfry = { "", "jeden", "dwa", "trzy", "cztery", "pięć", "sześć", "siedem", "osiem", "dziewięć" };
            string[] nastki = { "dziesięć", "jedenaście", "dwanaście", "trzynaście", "czternaście", "piętnaście", "szesnaście", "siedemnaście", "osiemnaście", "dziewiętnaście" };
            string[] rzedy = { "tysiąc", "tysiące", "tysięcy", "milion", "miliony", "milionów" };
            string setka = "";
            string wynik = "";
            int trojki = (kwota.ToString().Length - 1) / 3 + 1;
            for (int i = 1; i <= trojki; i++) {
                int rzad = 1;
                int jednosci = 0;
                int dziesiatki = 0;
                int setki = 0;
                while (kwota > 0 && rzad <= 3) {
                    int cyfra = kwota % 10;
                    switch (rzad) {
                        case 1:
                            setka = cyfry[cyfra];
                            jednosci = cyfra;
                            break;
                        case 2:
                            if (cyfra == 1) {
                                setka = nastki[jednosci];
                            }
                            else if (cyfra == 2) {
                                setka = "dwadzieścia " + setka;
                            }
                            else if (cyfra == 3) {
                                setka = "trzydzieści " + setka;
                            }
                            else if (cyfra == 4) {
                                setka = "czterdzieści " + setka;
                            }
                            else if (cyfra != 0) {
                                setka = cyfry[cyfra] + "dziesiąt " + setka;
                            }
                            dziesiatki = cyfra;
                            break;
                        case 3:
                            if (cyfra == 1) {
                                setka = "sto " + setka;
                            }
                            else if (cyfra == 2) {
                                setka = "dwieście " + setka;
                            }
                            else if (cyfra == 3 || cyfra == 4) {
                                setka = cyfry[cyfra] + "sta " + setka;
                            }
                            else if (cyfra != 0) {
                                setka = cyfry[cyfra] + "set " + setka;
                            }
                            setki = cyfra;
                            break;
                    }
                    rzad++;
                    kwota = kwota / 10;
                }
                if (i > 1 && setka != "") {
                    if (jednosci == 1 && dziesiatki == 0 && setki == 0) {
                        setka = rzedy[3 * i - 6];
                    }
                    else if (dziesiatki == 1 || jednosci >= 5 || jednosci <= 1) {
                        setka += " " + rzedy[3 * i - 4];
                    }
                    else if (jednosci > 1 && jednosci < 5) {
                        setka += " " + rzedy[3 * i - 5];
                    }
                }
                if (setka != "") {
                    wynik = setka + " " + wynik;
                }
            }
            return wynik.Substring(0, wynik.Length - 1);
        }
    }
}

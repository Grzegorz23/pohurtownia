﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Platnosc
    {
        [Key,ForeignKey("Zamowienie")]
        public int ZamowienieId { get; set; }
        public DateTime DataDoKiedy { get; set; }
        
        public int KlientId { get; set; }
        [ForeignKey("KlientId")]
        public virtual Klient Klient { get; set; }

        public int TypPlatnosciId { get; set; }
        [ForeignKey("TypPlatnosciId")]
        public virtual TypPlatnosci TypPlatnosci { get; set; }

        public bool Zaplacone { get; set; }
        
        public virtual Zamowienie Zamowienie { get; set; }
    }
}

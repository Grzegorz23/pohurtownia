﻿ using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hurtownia.Models
{   
    /// <summary>
    /// Repozytorium oferujące funkcje do komunikacji z bazą danych
    /// </summary>
    public class Repository
    {
        private EFDbContext ctx = new EFDbContext();

        /// <summary>
        /// Metoda pobierająca zamowienie o podanym ID z bazy danych
        /// </summary>
        /// <param name="id">Id zamowienia</param>
        /// <returns>Zamowienie</returns>
       public Zamowienie DajZamowienieOID(int id)
        {
            Zamowienie zamowienie;
            zamowienie = ctx.Zamowienia.Where(z => z.ZamowienieId == id)
                        .Include(z => z.PozycjezZamowienia)
                        .Include(z => z.Platnosc)
                        .Include(z => z.Platnosc.Klient.Adres)
                        .Include(z => z.Platnosc.Klient).SingleOrDefault();
            return zamowienie;
        } 
        /// <summary>
        /// Metoda zwracająca liste wszystkich pozycji zamówienia o wskazanym ID
        /// </summary>
        /// <param name="id">Id zamowienia</param>
        /// <returns> Lista pozycji zamówienia</returns>
        public IList<PozycjaZamowienia> DajPozycjeZamowieniaOID(int id)
        {
            Zamowienie zamowienie = DajZamowienieOID(id);
            IList<PozycjaZamowienia> pozycjeZamowienia = zamowienie.PozycjezZamowienia;
            return pozycjeZamowienia;
        }

        
        /// <summary>
        /// Metoda aktualizująca pozycje zamówienia o wskazanym id
        /// </summary>
        /// <param name="id">Id zamówienia</param>
        /// <param name="nowePozycje">Nowa lista pozycji</param>
        public void AktualizujPozycjeZamowieniaOID(int id,IList<PozycjaZamowienia> nowePozycje)
        {
            Zamowienie zamowienie = DajZamowienieOID(id);
            foreach(var pozycja in zamowienie.PozycjezZamowienia)
            {
              pozycja.IloscTowaru = nowePozycje.Where(p => p.Id == pozycja.Id).FirstOrDefault().IloscTowaru;
            }
            ctx.SaveChanges();
        }

        /// <summary>
        /// Metoda przypisująca adres dostawy do wskazanego zamówienia
        /// </summary>
        /// <param name="zamowienie">Zamówienie</param>
        /// <param name="adres">Adres dostawy zamówienia</param>
        public void PrzypiszAdresZamowieniu(Zamowienie zamowienie,Adres adres)
        {
            if (DajAdres(adres)==null)
            {
                DodajAdres(adres);
                zamowienie.AdresId = DajAdres(adres).Id;
            }
            else
                zamowienie.AdresId = DajAdres(adres).Id;
            ctx.SaveChanges();
        }

        /// <summary>
        /// Metoda zwracająca podany adres z bazy danych jeżeli istnieje
        /// </summary>
        /// <param name="adres">Adres</param>
        /// <returns>Adres</returns>
        public Adres DajAdres(Adres adres)
        {
            Adres returnAddress=ctx.Adresy
                .Where(a => a.Miasto == adres.Miasto &&
                a.KodPocztowy==adres.KodPocztowy &&
                a.NrMieszkania==adres.NrMieszkania &&
                a.NumerDomu==adres.NumerDomu &&
                a.Ulica==adres.Ulica)
                .FirstOrDefault();
            return returnAddress;
        }

        /// <summary>
        /// Metoda dodajaca nowy adres do bazy danych
        /// </summary>
        /// <param name="adres">Adres</param>
        public void DodajAdres(Adres adres)
        {

            ctx.Adresy.Add(adres);
            ctx.SaveChanges();
        }

        /// <summary>
        /// Metoda DodajPlatnoscZamowienia dodaje nową płatność do zamówienia, ustawiając jej datę na aktualną.
        /// </summary>
        /// <param name="zamowienie">Zamówienie, którego dotyczy płatność.</param>
        public void DodajPlatnoscZamowienia(Zamowienie zamowienie)
        {
            zamowienie.Platnosc = new Platnosc {
                DataDoKiedy = DateTime.Now,
                KlientId = zamowienie.Adres.Klienci.FirstOrDefault().Id
            };
        }

        /// <summary>
        /// Metoda UstawTypPlatnosciZamowieniaOID aktualizuje płatność, dotyczącą zamówienia o podanym id, określając jaka forma płatności została wybrana.
        /// </summary>
        /// <param name="id">Id zamówienia, którego dotyczy płatność.</param>
        /// <param name="platnosc">Wybrana forma płatności.</param>
        public void UstawTypPlatnosciZamowieniaOID(int id,string platnosc)
        {
            Zamowienie zamowienie = DajZamowienieOID(id);
            if (zamowienie.Platnosc == null)
                DodajPlatnoscZamowienia(zamowienie);
            zamowienie.Platnosc.TypPlatnosciId = (platnosc == "Gotówka" ? 1 : 2);
            zamowienie.Platnosc.Zaplacone = (platnosc== "Gotówka" ? false : true);
            ctx.SaveChanges();
        }

        /// <summary>
        /// Metoda DajListeZamowienZrealizowanych zwraca listę zamówień ze statusem "zrealizowane".
        /// </summary>
        /// <returns>Lista zamówień zrealizowanych.</returns>
        public IList<Zamowienie> DajListeZamowienZrealizowanych()
        {
            IList<Zamowienie> zamowienia = ctx.Zamowienia.Where(z => z.StatusZamowienia.Status == "Zrealizowane").ToList();
            return zamowienia;
        }

        /// <summary>
        /// Metoda DajFaktureOID zwraca fakturę o podanym id.
        /// </summary>
        /// <param name="id">Id zamówienia.</param>
        /// <returns>Faktura o podanym id.</returns>
        public Faktura DajFaktureOID(int id)
        {
            return ctx.Faktury
                    .Where(f => f.ZamowienieId == id)
                    .Include(z => z.DowodSprzedazy.PozycjezDowoduSprzedazy)
                    .Include(z => z.DowodSprzedazy.Zamowienie)
                    .Include(z => z.DowodSprzedazy.Zamowienie.Platnosc)
                     .SingleOrDefault();
        }

        /// <summary>
        /// Metoda UtworzFaktureZZamowienia dla podanego zamówienia tworzy nową fakturę, wiążąc ją z pozycjami faktury, tworzonymi na bazie pozycji zamówienia.
        /// </summary>
        /// <param name="zamowienie">Zamówienie, którego dotyczy faktura.</param>
        public void UtworzFaktureZZamowienia(Zamowienie zamowienie)
        {
            DowodSprzedazy dowodSprzedazy = new DowodSprzedazy {
                ZamowienieId = zamowienie.ZamowienieId,
                Zamowienie = zamowienie,
                DataWystawienia = DateTime.Now
            };

            var faktura = new Faktura {
                ZamowienieId = zamowienie.ZamowienieId,
                NIP = zamowienie.Platnosc.Klient.NIP,
                NazwaFirmy = zamowienie.Platnosc.Klient.NazwaFirmy,
                Imie = zamowienie.Platnosc.Klient.Imie,
                Nazwisko = zamowienie.Platnosc.Klient.Nazwisko,
                NumerDomu = zamowienie.Adres.NumerDomu,
                NumerMieszkania = zamowienie.Adres.NrMieszkania,
                Ulica = zamowienie.Adres.Ulica,
                Miasto = zamowienie.Adres.Miasto,
                KodPocztowy = zamowienie.Adres.KodPocztowy
            };

            List<PozycjaDowoduSprzedazy> pozycje = new List<PozycjaDowoduSprzedazy>();

            foreach (var pozycja in zamowienie.PozycjezZamowienia) {
                PozycjaDowoduSprzedazy pozycjaDowoduSprzedazy = new PozycjaDowoduSprzedazy {
                    DowodSprzedazyId = zamowienie.ZamowienieId,
                    IloscTowaru = pozycja.IloscTowaru,
                    NazwaTowaru = pozycja.Towar.NazwaTowaru,
                    Cena = pozycja.Towar.Cena,
                    CenaNetto = faktura.ObliczCeneNetto(pozycja.Towar.Cena, 0.23f)
                };
                pozycje.Add(pozycjaDowoduSprzedazy);
                ctx.PozycjeDowoduSprzedazy.Add(pozycjaDowoduSprzedazy);
            }
            dowodSprzedazy.PozycjezDowoduSprzedazy = pozycje;
            faktura.DowodSprzedazy = dowodSprzedazy;

            faktura.ZapiszSlownaKwote();
            ctx.DowodySprzedazy.Add(dowodSprzedazy);
            ctx.Faktury.Add(faktura);
            ctx.SaveChanges();
        }

        /// <summary>
        /// Metoda AktualizujWartoscZamowieniaOID aktualizuje wartość zamówienia o podanym id.
        /// </summary>
        /// <param name="id">Id zamówienia.</param>
        public void AktualizujWartoscZamowieniaOID(int id)
        {
            Zamowienie zamowienie = DajZamowienieOID(id);
            float suma = 0;
            foreach(var pozycja in zamowienie.PozycjezZamowienia) {
                suma += pozycja.IloscTowaru * pozycja.Towar.Cena;
            }
            zamowienie.WartoscZamowienia = suma;
            ctx.SaveChanges();
        }
    }
}
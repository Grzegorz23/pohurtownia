﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Hurtownia.Models
{
    public class Adres
    {
        public int Id { get; set; }
        public string Ulica { get; set; }
        public int NumerDomu { get; set; }
        public int NrMieszkania { get; set; }
        public string Miasto { get; set; }
        public string KodPocztowy { get; set; }

        public virtual ICollection<Klient> Klienci { get; set; }
        public virtual ICollection<Zamowienie> Zamowienia { get; set; }
    }
}

﻿using Hurtownia.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Hurtownia.Controllers
{
    /// <summary>
    /// Klasa Order Controller zarządza wszystkimi widokami związanymi z zamówieniami.
    /// </summary>
    public class OrderController : Controller
    {
        /// <summary>
        /// Instancja repozytorium
        /// </summary>
        private Repository repository = new Repository();

        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Widok koszyka z zamówieniam
        /// </summary>
        /// <returns>widok koszyka</returns>
        public ActionResult Cart()
        {
            Zamowienie zamowienie = repository.DajZamowienieOID(4);
            return View(zamowienie);
        }

        /// <summary>
        /// Metoda post, przy wyjściu z widoku koszyka sprawdza czy wskazana ilość towaru jest dostępna na magazynie, jeżeli nie to nie przepuszcza
        /// do następnego widoku. Jeżeli ilość towaru jest wystarczająca, przechodzi do podsumowania zamówienia.
        /// </summary>
        /// <param name="model">Zamówienie</param>
        /// <returns>Widok podsumowania bądź koszyka</returns>
        [HttpPost]
        public ActionResult Cart(Zamowienie model)
        {
            if (!ModelState.IsValid || !SprawdzDostepnoscTowarow(model.PozycjezZamowienia.ToList())) {
                return RedirectToAction("Cart");
            }
            Zamowienie zamowienie = repository.DajZamowienieOID(model.ZamowienieId);
            repository.AktualizujPozycjeZamowieniaOID(model.ZamowienieId, model.PozycjezZamowienia);
            repository.AktualizujWartoscZamowieniaOID(model.ZamowienieId);
            
            return RedirectToAction("Summary", new { id = model.ZamowienieId });
        }

        /// <summary>
        /// Metoda pokazująca widok podsumowania zamówienia
        /// </summary>
        /// <param name="id">Id Zamówienia</param>
        /// <returns>Widok podsumowania</returns>
        public ActionResult Summary(int id)
        {
            Zamowienie zamowienie = repository.DajZamowienieOID(id);
            return View(zamowienie);
        }

        /// <summary>
        /// Metoda zwracająca widok w którym wprowadza się szczegóły dotyczące dostawy
        /// </summary>
        /// <param name="id">Id Zamówienia</param>
        /// <returns>Widok szczegółów dostawy</returns>
        public ActionResult Edit(int id)
        {
            Zamowienie zamowienie = repository.DajZamowienieOID(id);
            return View(zamowienie);
        }
        /// <summary>
        /// Metoda post, przy wyjściu ze szczegółów dostawy sprawdza czy podane dane dotyczące adresu są prawidłowe, jeżeli nie
        /// to wraca do poprzedniego widoku wprowadzania. Jeżeli dane sa poprawne to przechodzi do wyboru płatności
        /// </summary>
        /// <param name="model">Zamowienie</param>
        /// <returns>Widok wprowadzania szczegółów dostawy lub widok wyboru płatności</returns>
        [HttpPost]
        public ActionResult Edit(Zamowienie model)
        {
            if (!ModelState.IsValid) {
                return RedirectToAction("Edit", model.ZamowienieId);
            }
            Zamowienie zamowienie = repository.DajZamowienieOID(model.ZamowienieId);
            repository.PrzypiszAdresZamowieniu(zamowienie, model.Adres);   
            
            return RedirectToAction("Payment", new { id=model.ZamowienieId });
        }

        /// <summary>
        /// Metoda post, przy wyjściu z widoku wyboru płatności, sprawdza czy została wybrana płatność, jeżeli nie to nie użytkownik zostaje przekierowany
        /// spowrotem do widoku wyboru płatności. Jeżeli płatność została wybrana - następuje aktualizacja zamówienia.
        /// </summary>
        /// <param name="paymentType">Wybrany typ płatności</param>
        /// <param name="zam">Zamówienie </param>
        /// <returns>Widok wyboru płatności lub powrót na strone główną (koszyk)</returns>
        [HttpPost]
        public ActionResult Payment(string paymentType,Zamowienie zam)
        {
            if (paymentType == null)
                return RedirectToAction("Payment");
            else
            {
                repository.UstawTypPlatnosciZamowieniaOID(zam.ZamowienieId, paymentType);
                return RedirectToAction("Cart");
            }
        }


        /// <summary>
        /// Widok wyboru płatności
        /// </summary>
        /// <param name="id">Id zamówienia</param>
        /// <returns>widok wyboru płatności</returns>
        public ActionResult Payment(int id)
        {
            return View(repository.DajZamowienieOID(id));
        }

        public ActionResult OrdersList()
        {
            return View(repository.DajListeZamowienZrealizowanych());
        }

        /// <summary>
        /// Metoda OrdersList dla akcji post obsługuje przejście z widoku listy zamówień do listy towarów dla wybranego zamówienia
        /// </summary>
        /// <param name="choosen">Id wybranego zamówienia</param>
        /// <returns>Przekierowanie do metody OrderProducts.</returns>
        [HttpPost]
        public ActionResult OrdersList(string choosen)
        {

            if (choosen == null) {
                return RedirectToAction("OrdersList");
            }
            int idZamowienia = Int32.Parse(choosen);
            return RedirectToAction("OrderProducts", new { id = idZamowienia });
        }

        /// <summary>
        /// Metoda OrderProducts wyświetla widok zawierający listę pozycji zamówienia o podanym id.
        /// </summary>
        /// <param name="id">Id wybranego zamówienia.</param>
        /// <returns>Widok listy pozycji zamówienia</returns>
        public ActionResult OrderProducts(int id)
        {
            return View(repository.DajZamowienieOID(id));
        }

        /// <summary>
        /// Metoda OrderProducts dla akcji post obsługuje przejście z widoku pozycji zamówienia do widoku faktury.
        /// </summary>
        /// <param name="zam">Zamówienie, którego dotyczą pozycje.</param>
        /// <returns>Przekierowanie do metody Invoice.</returns>
        [HttpPost]
        public ActionResult OrderProducts(Zamowienie zam)
        {
            return RedirectToAction("Invoice", new { id = zam.ZamowienieId });
        }

        /// <summary>
        /// Metoda Invoice za pomocą id wyszukuje zamówienie i wyświetla fakturę, która go dotyczy. Jeśli faktura dla danego zamówienia nie została jeszcze utworzona,
        /// metoda generuje ją.
        /// </summary>
        /// <param name="id">Id zamówienia, którego dotyczyć ma faktura.</param>
        /// <returns>Widok faktury.</returns>
        public ActionResult Invoice(int id)
        {
            Faktura faktura = repository.DajFaktureOID(id);
            if (faktura == null) {
                repository.UtworzFaktureZZamowienia(repository.DajZamowienieOID(id));
                faktura = repository.DajFaktureOID(id);
            }
            return View(faktura);
        }

        /// <summary>
        /// Metoda SprawdzDostepnoscTowarow sprawdza, czy dla podanych pozycji zamówienia ilość towarów znajdujących się w magazynie jest wystarczająca.
        /// </summary>
        /// <param name="pozycje">Lista pozycji zamówienia.</param>
        /// <returns>Wartość true, jeśli ilość towarów jest wystarczająca. W przeciwnym przypadku false.</returns>
        public bool SprawdzDostepnoscTowarow(List<PozycjaZamowienia> pozycje)
        {
            using(var ctx = new EFDbContext()) {
                foreach(var pozycja in pozycje) {
                    if(pozycja.IloscTowaru > pozycja.Towar.IloscNaMagazynie) {
                        return false;
                    }
                }
            }
            return true;
        } 
    }
}